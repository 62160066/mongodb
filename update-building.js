const mongoose = require('mongoose')
const Room = require('./models/Room')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  const newInformaticsBuilding = await Building.findById('621bac3a8522abf00c7fcdfe')
  const room = await Room.findById('621bac3a8522abf00c7fce03')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}
main().then(() => {
  console.log('Finish')
})
